# Truku

## Este bot tiene como función mantener actualizada la página del ENJM  en la parte de juegos actuales.

Esto se logra con dos comandos:
- `-empezar` seguido del nombre de un juego sirve para que, si en ese canal no se estaba jugando ese juego, quede persistido que x canal juega y juego. Si se llama cuando ya se estaba jugando un juego el nuevo nombre reemplaza al anterior.
- `-terminar` seguido del nombre de un juego sirve para que, si en el canal se estaba jugando un juego de ese nombre, sea removida esa información. Si se envía un nombre diferente a el que está guardado, Truku envía, entre otras cosas, el comando para efectivamente terminar el juego actual.

### Funcionamiento
Truku recibe todos los comandos y compara tres cosas:
 - *Si es un comando válido*: Estos son `-ayuda`, `-empezar`, `-terminar`, si el mensaje que llega no empieza con de alguna de esas tres formas, es ignorado.
 - *Si el servidor es el del `ENJM`*: En `secrets.py` hay definida una lista de servidores permitidos, de momento son el del `ENJM` y el mi servidor personal, para agregar uno nuevo, simplemente se agrega el id a la lista.
 - *Si el usuario que trate de hacer el comando tiene el rol necesario*: El comando `-ayuda` puede ser usado por cualquier usuario. En caso de que el comando sea uno de los otros dos se valida que el usuario tenga más de un rol. Esto está hecho así porque los usuarios normales no van a tener roles en este caso, mientras que hay muchos roles para los organizadores. De este modo toda la organización del `ENJM` puede empezar y terminar partidas. Si se quisiera se podría hacer una validación análoga a la del servidor para los roles.

Truku guarda las cosas en s3 a través de tres constantes guardadas en `secrets.py`:
- **BUCKET**: el bucket de s3
- **ACCESS_KEY**: la clave pública de acceso conseguida a través de la consola de aws
- **SECRET_KEY**: la clave secreta de acceso también conseguida a través de la consola de aws

Por último, para que Truku funcione hay que guardar el token que se consigue en [la consola de discord](https://discord.com/developers/applications/) (el bot -> Bot -> TOKEN -> Copiar/Copy) y guardarlo como TOKEN en `secrets.py`.