from s3_utils import info_from_s3, get_s3_client, upload_to_s3

KEY_NAME = 'ahora-mismo'


def get_response_for_game_starting(saved_game, game_name, channel_name):
		existing_game_string = ""
		if saved_game:
			existing_game_string = f" dejaron de jugar {saved_game} y"

		return f"Gracias por avisarme que en {channel_name}{existing_game_string} empezaron a jugar {game_name}"


def handle_game_start(saved_game, game_name, games, channel_id, channel_name):
    if saved_game == game_name:
        return { "games": None, "message": f"En {channel_name} ya estaban jugando {saved_game}, no cambié nada" }

    games[channel_id] = game_name

    return { "games": games, "message": get_response_for_game_starting(saved_game, game_name, channel_name) }


def handle_game_end(saved_game, game_name, games, channel_id, channel_name):
		if not saved_game:
			return { "games": None, "message": f"En {channel_name} no se estaba jugando nada, pero gracias de todas formas por avisarme" }

		if game_name == saved_game:
			games.pop(channel_id)
			
			return { "games": games, "message": f"Gracias por avisarme que en {channel_name} dejaron de jugar {saved_game}" }

		return { "games": games, "message": f'Trataste de terminar {game_name} y se estaba jugando {saved_game}, no soy tan inteligente para ver si son lo mismo.\nPara terminar el juego por favor mandá "-terminar {saved_game}".' }

handler_dict = {
	"-terminar": handle_game_end,
	"-empezar": handle_game_start
}


def generic_handler(channel_id, command, command_type, channel_name):
		s3_client = get_s3_client()

		content = info_from_s3(KEY_NAME, s3_client)

		game_name = command.split(command_type)[1].strip()
		games = content.get("juegos", "")

		saved_game = games.get(channel_id, "")

		to_save = handler_dict[command_type](saved_game, game_name, games, channel_id, channel_name)
		to_save_games = to_save["games"]
		message = to_save["message"]

		if to_save_games != None:
				content['juegos'] = to_save_games

				upload_to_s3(content, KEY_NAME, s3_client)

		return message