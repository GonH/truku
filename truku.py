
# bot.py
import os
import discord
import json
import logging

from handle_requests import generic_handler
from constants import EMPEZAR, TERMINAR
from secrets import TOKEN, VALID_CHATS

logger = logging.getLogger('discord')
logger.setLevel(logging.INFO)
client = discord.Client()

async def on_ready():
    logger.info(f'{client.user} has connected to Discord!')


def is_empezar_command(command):
    return command.startswith(EMPEZAR)


def is_terminar_command(command):
    return command.startswith(TERMINAR)


def is_invalid_command(command):
    return not is_empezar_command(command) and not is_terminar_command(command)


def is_invalid_chat(chat_id):
    return chat_id not in VALID_CHATS


@client.event
async def on_message(message):
    print(message)
    content = message.content
    server = message.author.guild.id
    channel_name = message.channel.name

    if content.startswith('-ayuda'):
        await message.channel.send(f'Solo se hacer dos cosas.\nSi me decís "-empezar" y el nombre de un juego, por ejemplo "-empezar Cultivos Mutantes", voy a anotar que en {channel_name} están jugando Cultivos Mutantes.\nPor otra parte si me decís "-terminar", por ejemplo "-terminar Cultivos Mutantes", voy a anotar que en {channel_name} se terminó de jugar a Cultivos Mutantes.\nEsta información solo sirve para que en la página del ENJM la gente pueda ver que juegos se están jugando ahora mismo.\nSi tenés mas dudas hacé una pregunta usando @Gon o leé la documentación en https://gitlab.com/GonH/truku/-/blob/master/README.md.')

    if is_invalid_command(content) or is_invalid_chat(server):
        return

    if len(message.author.roles) == 1:
        await message.channel.send(f'Perdón pero solo los organizadores del ENJM pueden tirarme comandos :frowning:.\nSi sos organizador y no te estoy respondiendo hablá con la comisión de logística.')
        return

    channel = f'{message.channel.id}'

    response = ""
    if is_empezar_command(content):
        response = generic_handler(channel, content, EMPEZAR, channel_name)
    else:
        response = generic_handler(channel, content, TERMINAR, channel_name)

    await message.channel.send(response)

client.run(TOKEN)
